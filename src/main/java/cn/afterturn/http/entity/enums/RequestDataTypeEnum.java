package cn.afterturn.http.entity.enums;

/**
 * 请求类型
 * @author  jue on 14-4-21.
 * @since 3.0
 */
public enum RequestDataTypeEnum {

    FORM,JSON;

}
